package com.example.lorencoo.sdaapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.Student;


@Database(entities = {Student.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract StudentDao studentDao();
}