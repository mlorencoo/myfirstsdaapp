package com.example.lorencoo.sdaapp.students.changeStudentData;

import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.Student;

public class  AddNewStudentPresenter  implements AddNewStudentContract.Presenter {
    private AddNewStudentContract.View view;
    private StudentDao studentDao;
    private boolean isEditMode;
    private Student student = new Student();

    public AddNewStudentPresenter(AddNewStudentContract.View view, StudentDao studentDao) {
        this.view = view;
        this.studentDao = studentDao;
    }

    @Override
    public void onSaveClick(String name, String surname, int grade, String accountValue, boolean isDrunk, String city, String street, String numberAddress) {
        if (isAddressValid(city, street, numberAddress)) {
            student.name=name;
            student.surname=surname;
            student.grade=grade;
            if(!accountValue.isEmpty()){
                student.accountValue=Float.valueOf(accountValue);
            }
            student.isDrunk=isDrunk;
            student.address.city=city;
            student.address.street=street;
            student.address.number=Integer.valueOf(numberAddress);

            if(isEditMode){
                if(studentDao.updateStudents(student)>=0){
                    view.close();
                }else {
                    view.showError();
                }
            }else {
                if(studentDao.insert(student)>=0){
                    view.close();
                }else {
                    view.showError();
                }
            }
        }else {
            view.showMissingAddress();
        }
    }
    private boolean isAddressValid(String city, String street, String homeNumber) {
        return !city.isEmpty() && !street.isEmpty() && !homeNumber.isEmpty();
    }

    @Override
    public void handleStudent(int studentId) {
        if (studentId >= 0) {
            // this id exist in the database, so get student and display its data
            isEditMode = true;
            student = studentDao.getStudentById(studentId);
            view.showStudentData(student);
        }
    }
}
