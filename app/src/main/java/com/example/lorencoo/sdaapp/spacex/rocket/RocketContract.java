package com.example.lorencoo.sdaapp.spacex.rocket;

import com.example.lorencoo.sdaapp.data.spacex_data.Rocket;

import java.util.List;

public interface RocketContract {

    interface View {

        void showError();

        void showData(List<Rocket> rockets);

        void showProgress();
    }

    interface Presenter {

        void onTryAgainClick();

        void doYourUpdate();
    }
}
