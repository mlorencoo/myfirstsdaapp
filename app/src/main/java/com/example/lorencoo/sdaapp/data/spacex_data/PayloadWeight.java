package com.example.lorencoo.sdaapp.data.spacex_data;

class PayloadWeight {
    public String id;
    public String name;
    public Integer kg;
    public Integer lb;
}
