package com.example.lorencoo.sdaapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lorencoo.sdaapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lorencoo on 22.02.2018.
 */

public class FirstFragment extends Fragment implements FragmentShowText {

    public static final String TAG = "FIRST_FRAGMENT";

    @BindView(R.id.fragment_first_text_view)
    TextView fragment_first_text_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void comunicate(String textToShow) {
        fragment_first_text_view.setText(textToShow);
    }

    @Override
    public void changeColor(int colorToChange) {
    fragment_first_text_view.setTextColor(colorToChange);
    }
}
