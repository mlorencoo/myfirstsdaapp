package com.example.lorencoo.sdaapp;

import javax.inject.Scope;

@Scope
public @interface ApplicationScope {
}
