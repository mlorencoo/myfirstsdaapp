package com.example.lorencoo.sdaapp.async;

import android.os.AsyncTask;

import timber.log.Timber;

public class MyAsyncTaskPrgresBar extends AsyncTask<Integer, Integer, String> implements BackgroundTaskContext, BackgroundTaskContext.Task {
    private BackgroundTaskContext.View view;

    public MyAsyncTaskPrgresBar(BackgroundTaskContext.View view) {
        this.view = view;
    }

    @Override
    protected void onPreExecute() {
        view.showProgres();
    }

    @Override
    protected String doInBackground(Integer... params) {
        int start = params[0];
        int stop = params[1];
        while (start < stop) {
            Timber.d(String.valueOf(start++));
            publishProgress(start);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return String.format("You've just counted to %d", stop);
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        view.updateProgress(values[0]);
        view.showOutput(String.valueOf(values[0]));
    }
    @Override
    protected void onPostExecute(String result) {
        view.hideProgres();
        view.showOutput(result);
    }

    @Override
    public void count(Integer from, Integer to) {
        execute(from, to);
    }

}
