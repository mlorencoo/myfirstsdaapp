package com.example.lorencoo.sdaapp.spacex.rocket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.SdaAplication;
import com.example.lorencoo.sdaapp.api.Api;
import com.example.lorencoo.sdaapp.data.spacex_data.Rocket;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RocketActivity extends AppCompatActivity implements RocketContract.View {
    @BindView(R.id.my_toolbar)
    android.support.v7.widget.Toolbar myToolbar;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;

    @BindView(R.id.spaces_rocket_reycler)
    RecyclerView rocketRecycler;

    @BindView(R.id.spacex_rocket_progress)
    ProgressBar rocketProgress;

    @BindView(R.id.spacex_rocket_error_group)
    Group rocketErrorGroup;


    @Inject
    RocketContract.Presenter presenter;

    @OnClick(R.id.spacex_rocket_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    private RocketAdapter rocketAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spacex_rockets);
        ButterKnife.bind(this);

        setupToolbar();

        ((SdaAplication) getApplication()).getAppComponent()
                .plusRocket(new RocketModule(this))
                .inject(this);

//        Retrofit retrofit = new Retrofit.Builder()
//                // set base url for every request method defined in Api.class
//                .baseUrl(Api.BASE_URL)
//                // add Gson converter to deserialize JSON into POJO
//                .addConverterFactory(GsonConverterFactory.create(new Gson()))
//                // return Retrofit instance
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .build();
//
//        presenter = new RocketPresenter(
//                this,
//                // create Api interface instance
//                retrofit.create(Api.class)
//        );

        rocketAdapter = new RocketAdapter();
        rocketRecycler.setLayoutManager(new LinearLayoutManager(this));
        rocketRecycler.setAdapter(rocketAdapter);

        swiperefresh.setOnRefreshListener(() -> presenter.doYourUpdate());
    }

    private void setupToolbar() {
        setSupportActionBar(myToolbar);
        myToolbar.setLogo(R.drawable.ic_cloud_black_48px);
        if (myToolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        myToolbar.setNavigationOnClickListener(view -> onBackPressed());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_rocket_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_item_refresh_rocket:
                presenter.doYourUpdate();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void showError() {
        rocketProgress.setVisibility(View.INVISIBLE);
        rocketErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Rocket> rockets) {
        swiperefresh.setRefreshing(false);
        rocketProgress.setVisibility(View.INVISIBLE);
        rocketRecycler.setVisibility(View.VISIBLE);
        rocketAdapter.updateRockets(rockets);
    }

    @Override
    public void showProgress() {
        rocketRecycler.setVisibility(View.INVISIBLE);
        rocketErrorGroup.setVisibility(View.INVISIBLE);
        rocketProgress.setVisibility(View.VISIBLE);
    }

}
