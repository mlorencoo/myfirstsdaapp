package com.example.lorencoo.sdaapp.students.changeStudentData;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {AddNewStudentModule.class})
public interface AddNewStudentComponent {
    void inject(AddNewStudent addNewStudent);
}
