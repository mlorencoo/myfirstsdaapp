package com.example.lorencoo.sdaapp;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by lorencoo on 26.02.2018.
 */

public class MyPageAdapter extends FragmentPagerAdapter {
    public MyPageAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.context = context;
        this.fragmentList = fragmentList;
    }

    private List<Fragment> fragmentList;
    private Context context;

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.page1);
            case 1:
                return context.getString(R.string.page2);
            default:
                return context.getString(R.string.page3);
        }
    }
}
