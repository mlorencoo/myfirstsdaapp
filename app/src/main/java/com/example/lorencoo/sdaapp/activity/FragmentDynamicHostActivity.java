package com.example.lorencoo.sdaapp.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.fragments.FirstFragment;
import com.example.lorencoo.sdaapp.fragments.SecondFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentDynamicHostActivity extends MenuItemsToSdaBar {

    private List<Fragment> fragmentList = new ArrayList<>();
    private boolean isFirstDisplay = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_dynamic_host);
        ButterKnife.bind(this);
        fragmentList.add(new FirstFragment());
        fragmentList.add(new SecondFragment());
        changeFragment(FirstFragment.TAG);
    }

    private void changeFragment(String tag) {
        Fragment fragmentToShow;
        if (tag.equals(FirstFragment.TAG)) {
            fragmentToShow = fragmentList.get(0);
            isFirstDisplay = true;
        } else {
            fragmentToShow = fragmentList.get(1);
            isFirstDisplay = false;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.dynamic_contianer, fragmentToShow)
                .commit();
    }

    @OnClick(R.id.dynamic_contoner_replace_button)
    public void pressReplaceFragment() {
        if (isFirstDisplay) {
            changeFragment(SecondFragment.TAG);
        } else
            changeFragment(FirstFragment.TAG);
    }
}
