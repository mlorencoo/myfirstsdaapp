package com.example.lorencoo.sdaapp.spacex.rocket;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.sdaapp.api.Api;
import com.example.lorencoo.sdaapp.data.spacex_data.CompanyInfo;
import com.example.lorencoo.sdaapp.data.spacex_data.Rocket;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RocketPresenter implements RocketContract.Presenter, LifecycleObserver {

    private RocketContract.View view;
    private Api api;

    private CompanyInfo companyInfo;
    private List<Rocket> rocketList;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RocketPresenter(RocketContract.View view, Api api) {
        this.view = view;
        this.api = api;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private void onCreate() {
        // todo get company info data
        getCompanyInfo();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getRockets();
        getLaunchesByYear(2018);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }

    @Override
    public void onTryAgainClick() {
        getRockets();
    }

    @Override
    public void doYourUpdate() {
        getRockets();
    }

    private void getCompanyInfo() {
        compositeDisposable.add(api.getCompanyInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        companyInfo -> {
                            this.companyInfo = companyInfo;
                            Timber.e(companyInfo.getHeadquarters().getAdrres());
                        },
                        throwable -> {
                            Timber.e(throwable.getMessage());
                        }, () -> {

                        }));

//        api.getCompanyInfo().enqueue(new Callback<CompanyInfo>() {
//            @Override
//            public void onResponse(Call<CompanyInfo> call, Response<CompanyInfo> response) {
//                // server responded to our call
//                if (response.isSuccessful()) {
//                    companyInfo = response.body();
//                    Timber.e(companyInfo.getHeadquarters().getAdrres());
//                } else {
//                    Timber.e(response.message());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CompanyInfo> call, Throwable t) {
//                // there was a connection error with server
//                Timber.e(t);
//            }
//        });
    }

    private void getRockets() {
        view.showProgress();
        compositeDisposable.add(api.getRockets()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        rocketList -> view.showData(rocketList),
                        throwable -> view.showError()
                ));


//        view.showProgress();
//        api.getRockets().enqueue(new Callback<List<Rocket>>() {
//            @Override
//            public void onResponse(Call<List<Rocket>> call, Response<List<Rocket>> response) {
//                if (response.isSuccessful()) {
//                    // we got data
//                    view.showData(response.body());
//                } else {
//                    // an error occurred - handle it
//                    view.showError();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Rocket>> call, Throwable t) {
//                // connection failed - handle it
//                view.showError();
//            }
//        });
    }
    private void getLaunchesByYear(int year){
        compositeDisposable.add(
                api.getLaunchesByYear(year)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                launches -> {
                                    if (!launches.isEmpty()){
                                        Timber.e(String.valueOf(launches.get(0).getFlightNumber()));
                                    }
                                },
                                Throwable::printStackTrace
                        )
        );

    }

}
