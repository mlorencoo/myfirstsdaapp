package com.example.lorencoo.sdaapp.students;

import com.example.lorencoo.sdaapp.data.student_data.Student;

import java.util.List;

public interface StudentsContract {

    interface View {
        void updateList(List<Student> students);

        void showStudentRemoved();

        void undoRemoval(int position, Student student);

        void showToast();
    }

    interface Presenter {

        void getStudentsFromRoom();

        void removeStudent(Student student,int position);

        void onSnackBarDismissed();

        void onSnackBarActionClick();

        void on2Click();

        void saveStudentCount();
    }

    interface OnClickListenerRemove {
        void removeStudent(Student student,int position);
    }

    interface Listener2 {
        void onClick(Student student);
    }
}
