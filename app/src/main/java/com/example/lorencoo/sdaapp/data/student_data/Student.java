package com.example.lorencoo.sdaapp.data.student_data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Student {
    @PrimaryKey(autoGenerate = true)
    public int id;


    @ColumnInfo(name = "first_name")
    public String name;

    @ColumnInfo(name = "last_name")
    public String surname;

    @ColumnInfo(name = "is_drunk")
    public boolean isDrunk;

    public int grade;

    @ColumnInfo(name = "account_value")
    public float accountValue;

    @Embedded
    public Address address=new Address();

}
