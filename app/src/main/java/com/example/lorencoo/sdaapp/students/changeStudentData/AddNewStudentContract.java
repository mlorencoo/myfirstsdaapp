package com.example.lorencoo.sdaapp.students.changeStudentData;

import com.example.lorencoo.sdaapp.data.student_data.Student;

public interface AddNewStudentContract {

    interface View {

        void  showStudentData(Student student);

        void showMissingAddress();

        void close();

        void showError();
    }

    interface Presenter {


        void handleStudent(int studentId);

        void onSaveClick(String name, String surname, int grade, String accountValue
                , boolean isDrunk, String city, String street, String numberAddress);
    }


}
