package com.example.lorencoo.sdaapp.activity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;

public class WebviewActivity extends MenuItemsToSdaBar {
    WebView wb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        String urlFromMainActivity = getIntent().getStringExtra(MainActivity.KEY_DATA);

        wb=(WebView)findViewById(R.id.webView);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.getSettings().setLoadWithOverviewMode(true);
        wb.getSettings().setUseWideViewPort(true);
        wb.getSettings().setBuiltInZoomControls(true);
        wb.setWebViewClient(new WebViewClient());
        wb.loadUrl("http://"+urlFromMainActivity);
    }
}
