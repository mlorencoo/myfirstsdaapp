package com.example.lorencoo.sdaapp.menues;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.activity.CameraActivity;
import com.example.lorencoo.sdaapp.activity.MainActivity;
import com.example.lorencoo.sdaapp.activity.SecondActivity;
import com.example.lorencoo.sdaapp.activity.callActivity;
import com.example.lorencoo.sdaapp.activity.razDwaCzyActivity;

/**
 * Created by lorencoo on 08.03.2018.
 */

public abstract class MenuItemsWithMail extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_item_main_activity == item.getItemId()) {
            openMenuMainActivity();
        } else if (R.id.menu_item_second_activity == item.getItemId()) {
            openMenuSecondActivity();
        }else if (R.id.menu_item_camera_activity == item.getItemId()) {
            openMenuCameraActivity();
        } else if (R.id.menu_item_raz_dwa == item.getItemId()) {
            openMenuSnowReview();
        }else if (R.id.menu_item_call_to == item.getItemId()) {
            openMenuCallToSb();
        }
        return true;
    }
    private void openMenuMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }
    private void openMenuSecondActivity() {
        startActivity(new Intent(this, SecondActivity.class));
    }
    public void openMenuCameraActivity() {
        startActivity(new Intent(this, CameraActivity.class));
    }
    private void openMenuSnowReview() {
        startActivity(new Intent(this, razDwaCzyActivity.class));
    }
    private void openMenuCallToSb() {
        startActivity(new Intent(this, callActivity.class));
    }

}
