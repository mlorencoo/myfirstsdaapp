package com.example.lorencoo.sdaapp.students.changeStudentData;

import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.PrefsManager;
import com.example.lorencoo.sdaapp.students.StudentsContract;
import com.example.lorencoo.sdaapp.students.StudentsPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AddNewStudentModule {

    private AddNewStudentContract.View view;

    public AddNewStudentModule(AddNewStudentContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    AddNewStudentContract.Presenter provideAddNewStudentPresenter(StudentDao studentDao ){
        return new AddNewStudentPresenter(view,studentDao);
    }
}
