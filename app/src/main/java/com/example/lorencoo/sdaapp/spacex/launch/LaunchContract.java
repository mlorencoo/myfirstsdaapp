package com.example.lorencoo.sdaapp.spacex.launch;

import com.example.lorencoo.sdaapp.data.spacex_data.Launch;

import java.util.List;

public interface LaunchContract {
    interface Presenter {
        void setRocketId(String rocketId);
    }

    interface View {
        void showLaunches(List<Launch> launches);

        void showError();
    }
}
