package com.example.lorencoo.sdaapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.lorencoo.sdaapp.async.MyAsyncTask;

import timber.log.Timber;

/**
 * Created by lorencoo on 02.03.2018.
 */

public class FirstService extends Service {

    private HandlerThread handlerThread;
    private boolean shouldRun = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("Never ending services created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        shouldRun = true;
        startCounting();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shouldRun = false;
        handlerThread.quitSafely();
        Timber.d("Never ending services destroyed");
    }

    private void startCounting() {
        new MyAsyncTask().execute(2);
//        handlerThread = new HandlerThread("Custom handler thread");
//        handlerThread.start();
//
//        Handler handler = new Handler(handlerThread.getLooper());
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                int i = 0;
//                while (shouldRun && i < 40) {
//                    Timber.d("%s Never ending services counted", String.valueOf(i++));
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                stopSelf();
//            }
//        });
    }

}
