package com.example.lorencoo.sdaapp.scheluders;

import android.app.job.JobInfo;

public class MyschedulerPresenter implements MySceduleContract.Presenter {
    private MySceduleContract.View view;

    public MyschedulerPresenter(MySceduleContract.View view) {
        this.view = view;
    }

    @Override
    public void onButtonClick() {
        view.scheduleJob();
    }
}
