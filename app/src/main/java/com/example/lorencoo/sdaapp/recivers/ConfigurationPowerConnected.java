package com.example.lorencoo.sdaapp.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by lorencoo on 27.02.2018.
 */

public class ConfigurationPowerConnected extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String name;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            name = context.getResources().getConfiguration().getLocales().get(0).getDisplayName();
        }
        else {
            name=context.getResources().getConfiguration().locale.getDisplayName();
        }
        Toast.makeText(context,name,Toast.LENGTH_SHORT).show();
    }
}

