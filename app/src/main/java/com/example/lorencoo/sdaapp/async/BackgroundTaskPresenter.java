package com.example.lorencoo.sdaapp.async;

import com.example.lorencoo.sdaapp.async.BackgroundTaskContext;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class BackgroundTaskPresenter implements BackgroundTaskContext.Presenter {
    private BackgroundTaskContext.View view;

    private Disposable disposable;

    private String output="";
    private Integer value;

    public BackgroundTaskPresenter(BackgroundTaskContext.View view) {
        this.view = view;
    }


    @Override
    public void countWithRxJava(Integer from, Integer to) {
        view.showProgres();
        disposable = countingObservable(from, to)
                .flatMap(integer -> {
                    value=integer;
                    return stringObservable();
                })
                .flatMap(string -> Observable.just(string+" to pedal xD"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        text ->output +=text+" ",
                        Throwable::printStackTrace,
                        ()->{
                            view.showOutput(output+" "+value.toString());
                            view.hideProgresRxJava();
                        }
                );
    }

    @Override
    public void clear() {
        disposable.dispose();
    }

    private io.reactivex.Observable<Integer> countingObservable(final Integer from, Integer to) {
        return io.reactivex.Observable.fromCallable(() -> {
            Integer start = from;
            while (start < to) {
                Timber.d(String.valueOf(start++));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return start;
        });
    }
    private Observable<String> stringObservable(){
        return Observable.just("Natonik","Mitoraj","Kepa");
    }
}
