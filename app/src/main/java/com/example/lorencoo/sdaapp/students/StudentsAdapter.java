package com.example.lorencoo.sdaapp.students;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.data.student_data.Student;
import com.example.lorencoo.sdaapp.students.changeStudentData.AddNewStudent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.StudentHolder> {

    private StudentsContract.OnClickListenerRemove removeListener;
    private StudentsContract.Listener2 listener2;
    private List<Student> studentList = new ArrayList<>();
    private int selectedItemPosition = -1;

    public StudentsAdapter(StudentsContract.OnClickListenerRemove removeListener, StudentsContract.Listener2 listener2) {
        this.removeListener = removeListener;
        this.listener2 = listener2;
    }


    public void updateStudentList(List<Student> students) {
        studentList.clear();
        studentList.addAll(students);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context  context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View studentView = layoutInflater.inflate(R.layout.row_layout_recycler_view, parent, false);

        return new StudentHolder(studentView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentsAdapter.StudentHolder studentHolder, int position) {

        studentHolder.setupStudent(studentList.get(position), removeListener, listener2);

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void undoRemoval(int position, Student student) {
        studentList.add(position, student);
        notifyItemInserted(position);
    }

    public class StudentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.student_id)
        TextView student_id;

        @BindView(R.id.cancel_student)
        Button cancel_student;

        public StudentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupStudent(Student student,
                                 StudentsContract.OnClickListenerRemove listenerRemove,
                                 StudentsContract.Listener2 listener2) {

            if (selectedItemPosition == getAdapterPosition()) {
                itemView.setBackgroundColor(Color.GREEN);

            } else {
                itemView.setBackgroundColor(Color.WHITE);
            }

            student_id.setText(String.format("%s %s %s", String.valueOf(student.id), student.name, student.surname));

            cancel_student.setOnClickListener(view -> {
                if (getAdapterPosition() == selectedItemPosition) {
                    selectedItemPosition = -1;
                }
                studentList.remove(student);
                listenerRemove.removeStudent(student,getAdapterPosition());
                listener2.onClick(student);
                notifyItemRemoved(getAdapterPosition());
            });

            itemView.setOnClickListener(view -> {
//                // get position of a clicked item
//                int currentPosition = getAdapterPosition();
//
//                if (currentPosition == selectedItemPosition) {
//                    // clicked item is the selected one, deselect it
//                    selectedItemPosition = -1;
//                    notifyItemChanged(currentPosition);
//                } else {
//                    // deselect previous item, and select this one
//                    final int previouslySelectedItem = selectedItemPosition;
//                    selectedItemPosition = -1;
//                    notifyItemChanged(previouslySelectedItem);
//                    selectedItemPosition = currentPosition;
//                    notifyItemChanged(currentPosition);
//                }
                Intent intent = new Intent(itemView.getContext(), AddNewStudent.class);
                intent.putExtra("idikStudenta", student.id);
                itemView.getContext().startActivity(intent);
            });
        }

    }
}
