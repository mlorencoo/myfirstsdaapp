package com.example.lorencoo.sdaapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.menues.MenuItemsWithMail;
import com.example.lorencoo.sdaapp.services.ForegroundService;
import com.example.lorencoo.sdaapp.services.ServicesCountTo30;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class SecondActivity extends MenuItemsWithMail {

    private String textSendFromIntent;

    @BindView((R.id.textViewProcentProgress))
    TextView textViewProcentProgress;
    @BindView((R.id.editTextAdresMail))
    EditText editTextAdresMail;
    @BindView((R.id.editTextSubjectMail))
    EditText editTextSubjectMail;
    @BindView((R.id.textView))
    TextView textView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private BroadcastReceiver getLocalBroadcastSendCounterToProgresBar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onLocalBroadcastcounterToProgresBar(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Timber.d("onCreate");
        ButterKnife.bind(this);

        getTextSendFromIntent();

//        registerMyBroadcastReciver();

        registerForLocalReceiverFromServiceToProgressBar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        unregisterMyBroadcastReciver();
        unregisterForLocalReceiverFromServiceToProgressBar();
    }

    @OnClick(R.id.button2)
    public void openWebBrowser() {
        String uri = textSendFromIntent;
        if (!textSendFromIntent.startsWith("http://")
                && !textSendFromIntent.startsWith("https://")) {
            uri = "http://" + uri;
        }
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        }
    }

    @OnClick(R.id.btOpenPages)
    public void openPagesActivity() {
        startActivity(new Intent(this, PagesActivity.class));
    }

    @OnClick(R.id.btOpenSBPB)
    public void setServicesCountTo30() {
        startService(new Intent(this, ServicesCountTo30.class));
    }

    @OnClick(R.id.buttonSendEmail)
    public void openEmialApp() {
        startActivity(Intent.createChooser(getShareIntent(), "Send mail..."));
    }
    @OnClick(R.id.btForegroundServiceNotfication)
    public void openForegroundServicewithNotfication() {
        Intent foregroundServiceintent=new Intent(this, ForegroundService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(foregroundServiceintent);
        } else
            startService(foregroundServiceintent);
    }

    private void getTextSendFromIntent() {
        Intent intent = getIntent();
        textSendFromIntent = intent.getStringExtra(MainActivity.KEY_DATA);
        textView.setText(textSendFromIntent);
    }

    private Intent getShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, editTextSubjectMail.getText().toString());
        String[] AdresMail = {editTextAdresMail.getText().toString()};
        sendIntent.putExtra(Intent.EXTRA_EMAIL, AdresMail);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
        sendIntent.setType("text/plain");
        return sendIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        shareActionProvider.setShareIntent(getShareIntent());
        return true;
    }

//    private void registerMyBroadcastReciver() {
//        IntentFilter filter = new IntentFilter();
//        this.registerReceiver(new MysendReciverBroadcast(), filter);
//    }
//    private void unregisterMyBroadcastReciver() {
//        this.unregisterReceiver(new MysendReciverBroadcast());
//    }

    private void registerForLocalReceiverFromServiceToProgressBar() {
        IntentFilter intentFilterrFromServiceToProgressBar = new IntentFilter(ServicesCountTo30.BROADCAST_ACTION_COUNT_SERVICE);
        LocalBroadcastManager.getInstance(this).registerReceiver(getLocalBroadcastSendCounterToProgresBar, intentFilterrFromServiceToProgressBar);
    }

    private void unregisterForLocalReceiverFromServiceToProgressBar() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(getLocalBroadcastSendCounterToProgresBar);
    }

    private void onLocalBroadcastcounterToProgresBar(Intent intent) {
        int progress = intent.getIntExtra(ServicesCountTo30.BROADCAST_COUNT_KEY, 0);
        float procentProgress = ((float) progress / (float) ServicesCountTo30.countTo) * 100;
        String prcnt = String.format("%.2f", procentProgress);
        textViewProcentProgress.setText(String.valueOf(prcnt + " %"));
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setMax(ServicesCountTo30.countTo);
        progressBar.setProgress(progress);
    }


}
