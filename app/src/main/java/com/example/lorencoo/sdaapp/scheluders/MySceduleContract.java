package com.example.lorencoo.sdaapp.scheluders;

import android.app.job.JobInfo;
import android.view.View;

public interface MySceduleContract {
    interface View {
        void scheduleJob();
    }

    interface Presenter {
        void onButtonClick();
    }
}
