package com.example.lorencoo.sdaapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lorencoo.sdaapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lorencoo on 22.02.2018.
 */

public class SecondFragment extends Fragment implements FragmentShowText {

    public static final String TAG = "SECOND_FRAGMENT";

    @BindView(R.id.fragment_second_text_view)
    TextView fragment_second_text_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void comunicate(String textToShow) {
        Toast.makeText(getActivity(),textToShow,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void changeColor(int colorToChange) {
        fragment_second_text_view.setBackgroundColor(colorToChange);
    }
}
