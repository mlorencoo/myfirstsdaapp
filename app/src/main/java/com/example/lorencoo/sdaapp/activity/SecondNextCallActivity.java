package com.example.lorencoo.sdaapp.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;

public class SecondNextCallActivity extends MenuItemsToSdaBar {
    public static final String KEY_DATA = "KEY_DATA";
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 23;
    public EditText editText;
    public boolean isChecked;
    private int counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_next_call);
        isChecked = getIntent().getBooleanExtra(KEY_DATA, false);
        editText = (EditText) findViewById(R.id.editTextEcondNextCall);
    }

    public void callToSomeone(View view) {
        if (isChecked) {
            int permissionCheckCallPhone = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
            if (permissionCheckCallPhone == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + editText.getText().toString()));
                startActivity(callIntent);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                    showDialog();
                } else {
                    requestCallPhonePermissions();
                }
            }
        } else {
            startActivity(new Intent(Intent.ACTION_DIAL));
        }
    }

    public void showDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.titleDialog)
                .setMessage(R.string.messegeDialog)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with permison
                        requestCallPhonePermissions();
                        counter++;
                        if (counter >= 3) {
                            requestCallPhonePermissions();
                            startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null)));
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void requestCallPhonePermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                MY_PERMISSIONS_REQUEST_CALL_PHONE);
    }
}
