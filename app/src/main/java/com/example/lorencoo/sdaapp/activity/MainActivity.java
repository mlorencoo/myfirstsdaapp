package com.example.lorencoo.sdaapp.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.services.FirstIntentServices;
import com.example.lorencoo.sdaapp.services.FirstService;
import com.example.lorencoo.sdaapp.services.ForegroundService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class MainActivity extends MenuItemsToSdaBar {
    public static final String KEY_DATA = "KEY_DATA";
    public static final String SEVER_INPUT = "SEVER_INPUT";

    private static final int SPLASH_DISPLAY_LENGTH = 10 * 1000;

    public static final String BROADCAST_ACTION = "com.example.lorencoo.sdaapp.activity.CUSTOM_ACTION";
    String BROADCAST_KEY = "key used to send string via broadcast";

    private BroadcastReceiver localBroadcastSentFromFragmentXml = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onLocalBroadcastReciver(context, intent);
        }
    };

    @BindView(R.id.button)
    Button button;
    @BindView(R.id.blinkButton)
    Button blinkButton;
    @BindView(R.id.buttonWebView)
    Button buttonWebView;
    @BindView(R.id.buttonCameraActivity)
    Button buttonCameraActivity;
    @BindView(R.id.buttonCall)
    Button buttonCall;
    @BindView(R.id.buttonFragmentDynimic)
    Button buttonFragment;
    @BindView(R.id.edBroadcastSend)
    EditText edBroadcastSend;
    @BindView(R.id.editText)
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Timber.d("onCreate");

        blinkedButton();

        if (savedInstanceState != null) {
            button.setBackgroundColor(savedInstanceState.getInt(SEVER_INPUT));
        }
        registerForLocalReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Timber.d("onStart");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Timber.d("onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this,FirstService.class));
        stopService(new Intent(this,FirstIntentServices.class));
        stopService(new Intent(this,ForegroundService.class));
        unregisterLocalReceiver();
        Timber.d("onDestroy");
    }

    @OnClick(R.id.buttonCameraActivity)
    public void openCameraActivity() {
        Intent cameraActivityIntent = new Intent(this, CameraActivity.class);
        startActivity(cameraActivityIntent);
    }
    @OnClick(R.id.blinkButton)
    public void openBlinkedButton() {
        startActivity(new Intent(this, razDwaCzyActivity.class));
    }
    @OnClick(R.id.buttonCall)
    public void openCallActivity() {
        startActivity(new Intent(this, callActivity.class));
    }
    @OnClick(R.id.buttonWebView)
    public void openWebVebWiew() {
        Intent intentWebView = new Intent(this, WebviewActivity.class);// lub this w wyzszej metodie lub swtworzyc nowa prywatna metode void
        intentWebView.putExtra(KEY_DATA, editText.getText().toString());
        startActivity(intentWebView);
    }
    @OnClick(R.id.button)
    public void openSecondActivityButton(View view) {
        Intent intent = new Intent(getApplicationContext(), SecondActivity.class);// lub this w wyzszej metodie lub swtworzyc nowa prywatna metode void
        intent.putExtra(KEY_DATA, editText.getText().toString());
        startActivity(intent);
        view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
    }
    @OnClick(R.id.buttonFragmentDynimic)
    public void openFragmentHostActivity() {
        startActivity(new Intent(this, FragmentDynamicHostActivity.class));
    }
    @OnClick(R.id.buttonFragmentXml)
    public void openFragmnetStaticHostActivity() {
        startActivity(new Intent(this, FragmentStaticHostActivity.class));
    }
    @OnClick(R.id.buttonOpenScroolview)
    public void openScroolviewActivity(){
        startActivity(new Intent(this,ScroolviewActivity.class));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ColorDrawable colorDrawable = (ColorDrawable) button.getBackground();
        outState.putInt(SEVER_INPUT, colorDrawable.getColor());
        super.onSaveInstanceState(outState);
    }

    private void registerForLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastSentFromFragmentXml, intentFilter);
    }
    private void unregisterLocalReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localBroadcastSentFromFragmentXml);
    }
    private void onLocalBroadcastReciver(Context context, Intent intent) {
        edBroadcastSend.setText(intent.getStringExtra(BROADCAST_KEY));
        createAlarmManager(context);
//        createHendlerPostDelay();
    }
    private void createHendlerPostDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent1 = new Intent(getApplicationContext(), SecondActivity.class);
                MainActivity.this.startActivity(intent1);
                MainActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
    private void createAlarmManager(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intentAlrm = new Intent(ALARM_SERVICE);
        intentAlrm.setClass(this, SecondActivity.class);
        PendingIntent alarmIntent = PendingIntent.getActivity(context, 0, intentAlrm, 0);

        alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 6 * 1000, alarmIntent);
    }

    private void blinkedButton() {
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        animation.setStartOffset(100);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);
        blinkButton.startAnimation(animation);
    }
}
