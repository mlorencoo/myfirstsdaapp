package com.example.lorencoo.sdaapp.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.drawer.DrawerActivity;
import com.example.lorencoo.sdaapp.files.ReadWriteFilesActivity;
import com.example.lorencoo.sdaapp.students.StudentActivity;
import com.example.lorencoo.sdaapp.async.BackgroundTaskContext;
import com.example.lorencoo.sdaapp.async.BackgroundTaskPresenter;
import com.example.lorencoo.sdaapp.async.MyAsyncTaskPrgresBar;
import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.scheluders.MyJobSchedulerActivity;
import com.example.lorencoo.sdaapp.services.BinderService;
import com.example.lorencoo.sdaapp.services.FirstIntentServices;
import com.example.lorencoo.sdaapp.services.FirstService;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lorencoo on 15.02.2018.
 */

public class ScroolviewActivity extends MenuItemsToSdaBar implements BackgroundTaskContext.View {
    private BackgroundTaskContext.Task task;
    private BackgroundTaskContext.Presenter presenter;
    public static final int START = 10;
    public static final int STOP = 15;
    private BinderService binderService;
    private boolean isServiceBound;
    int counterClick = 0;
    @BindView(R.id.progressBarAsync)
    ProgressBar progressBarAsync;
    @BindView(R.id.tvBarAsync)
    TextView tvBarAsync;
    @BindView(R.id.btPBAsync)
    Button btPBAsync;

    private ServiceConnection serviceConection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {
            BinderService.MyBinder binder = (BinderService.MyBinder) iBinder;
            binderService = binder.getService();
            isServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceBound = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroolview);
        ButterKnife.bind(this);
        task = new MyAsyncTaskPrgresBar(this);
        presenter = new BackgroundTaskPresenter(this);
        progressBarAsync.setMax(STOP);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindToService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unBindFromService();
        if (counterClick != 0) {
            presenter.clear();
        }
    }

    @OnClick(R.id.buttonFirstInrentService)
    public void openFirstIntentService() {
        startService(new Intent(this, FirstIntentServices.class));
    }

    @OnClick(R.id.buttonFirstService)
    public void openFirstService() {
        startService(new Intent(this, FirstService.class));
    }

    @OnClick(R.id.btNumberBindService)
    public void generateNumberWithBindService() {
        if (isServiceBound) {
            Toast.makeText(this, binderService.greet(), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btPBAsync)
    public void startProgressBarAsync() {

        task.count(START, STOP);
    }

    @OnClick(R.id.rxJavaButton)
    public void startRxJavaCounting() {
        counterClick++;
        presenter.countWithRxJava(START, STOP);
    }

    @OnClick(R.id.btJobSche)
    public void openJobSchedulerActivity() {
        startActivity(new Intent(this, MyJobSchedulerActivity.class));
    }

    @OnClick(R.id.btRecyclerView)
    public void openRecyclerViewActivity() {
        startActivity(new Intent(this, StudentActivity.class));
    }

    @OnClick(R.id.btSpacexRocket)
    public void openSpacexRocket() {
        startActivity(new Intent(this, RocketActivity.class));
    }

    private void bindToService() {
        bindService(new Intent(this, BinderService.class), serviceConection, Context.BIND_AUTO_CREATE);
    }

    @OnClick(R.id.btTextInputLayout)
    public void openTextInputLayout() {
        startActivity(new Intent(this, TextInputLayoutActivity.class));
    }

    @OnClick(R.id.btDrawer)
    public void openDrawer() {
        startActivity(new Intent(this, DrawerActivity.class));
    }

    @OnClick(R.id.btBazaDanych)
    public void openBazaDanych() {
        startActivity(new Intent(this, ReadWriteFilesActivity.class));
    }


    private void unBindFromService() {
        unbindService(serviceConection);
    }

    @Override
    public void showProgres() {
        progressBarAsync.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void hideProgres() {
        progressBarAsync.setVisibility(android.view.View.INVISIBLE);
        tvBarAsync.setText("");
        btPBAsync.setVisibility(android.view.View.INVISIBLE);
    }

    @Override
    public void hideProgresRxJava() {
    }

    @Override
    public void updateProgress(Integer progress) {
        progressBarAsync.setProgress(progress);
        tvBarAsync.setText(String.valueOf(progress));
    }

    @Override
    public void showOutput(String output) {
        tvBarAsync.setText(output);
    }
}
