package com.example.lorencoo.sdaapp.di;

import com.example.lorencoo.sdaapp.ApplicationScope;
import com.example.lorencoo.sdaapp.spacex.launch.LaunchComponent;
import com.example.lorencoo.sdaapp.spacex.launch.LaunchModule;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketComponent;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketModule;
import com.example.lorencoo.sdaapp.students.StudentComponent;
import com.example.lorencoo.sdaapp.students.StudentModule;
import com.example.lorencoo.sdaapp.students.changeStudentData.AddNewStudentComponent;
import com.example.lorencoo.sdaapp.students.changeStudentData.AddNewStudentModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent {
    StudentComponent plus(StudentModule studentModule);
    AddNewStudentComponent plusAddNew(AddNewStudentModule addNewStudentModule );
    RocketComponent plusRocket(RocketModule rocketModule );
    LaunchComponent plusLaunch(LaunchModule launchModule );

}
