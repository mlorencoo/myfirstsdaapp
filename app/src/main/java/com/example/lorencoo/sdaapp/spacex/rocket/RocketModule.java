package com.example.lorencoo.sdaapp.spacex.rocket;

import com.example.lorencoo.sdaapp.api.Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RocketModule {

    private RocketContract.View view;

    public RocketModule(RocketContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    RocketContract.Presenter provideRocketPresenter(Retrofit retrofit ){
        return new RocketPresenter(view, retrofit.create(Api.class));
    }
}
