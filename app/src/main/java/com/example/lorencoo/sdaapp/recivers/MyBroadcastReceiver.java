package com.example.lorencoo.sdaapp.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by lorencoo on 27.02.2018.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "MyBroadcastReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean status = intent.getBooleanExtra("state", false);
        if (status) {
            String log = ("Action: " + intent.getAction() + "\n") +
                    "URI: " + intent.toUri(Intent.URI_INTENT_SCHEME) + "\n";
            Log.d(TAG, log);
        }
    }
}
