package com.example.lorencoo.sdaapp.students;

import android.util.SparseArray;

import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.PrefsManager;
import com.example.lorencoo.sdaapp.data.student_data.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsPresenter implements StudentsContract.Presenter {
    private List<Student> students = new ArrayList<>();
    private StudentsContract.View view;
    private String name = "student number ";
    private SparseArray<Student> removedStudent = new SparseArray<>();
    private boolean shouldUndo = false;
    private PrefsManager prefsManager;
    private StudentDao studentDao;


    public StudentsPresenter(StudentsContract.View view, PrefsManager prefsManager, StudentDao studentDao) {
        this.view = view;
        this.prefsManager=prefsManager;
        this.studentDao= studentDao;
    }


    @Override
    public void getStudentsFromRoom() {
        view.updateList(studentDao.getAll());
//        students.clear();
//        int howManyStudents = prefsManager.getStudentsCount();
//        for (int i = 0; i < howManyStudents; i++) {
//            students.add(new Student());
//        }
//        view.updateList(students);
//
//        new GetCompanyInfoTask().execute(SpacexAPI.COMPANY_INFO);
//
//        new GetRocketInfoTask().execute(SpacexAPI.ROCKET_INFO);

    }

    @Override
    public void removeStudent(Student student,int position) {
        removedStudent.append(position, student);

        view.showStudentRemoved();
    }

    @Override
    public void onSnackBarActionClick() {
        shouldUndo = true;
    }

    @Override
    public void onSnackBarDismissed() {
        int position = removedStudent.keyAt(0);

        Student student = removedStudent.valueAt(0);
        if (shouldUndo) {
            shouldUndo = false;
            removedStudent.clear();
            view.undoRemoval(position, student);
        }
        else {
            studentDao.delete(student);
        }
    }

    @Override
    public void on2Click() {
        view.showToast();
    }

    @Override
    public void saveStudentCount() {
        prefsManager.savedStudentCount(students.size());
    }
}
