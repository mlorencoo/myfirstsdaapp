package com.example.lorencoo.sdaapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import timber.log.Timber;

/**
 * Created by lorencoo on 02.03.2018.
 */

public class FirstIntentServices extends IntentService {

    public FirstIntentServices() {
        super("firstIntentServices");
        Timber.d("Constructor");
//        Log.d("IntentService","Constructor");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate");
//        Log.d("IntentService","onCreate");

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Timber.d("onStartCommand");
//        Log.d("IntentService","onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Timber.d("onBind");
//        Log.d("IntentService","onBind");
        return super.onBind(intent);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Timber.d("Now intentService is working");
        for (int i = 0; i < 10; i++) {
            Timber.d(String.valueOf(i+1));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
