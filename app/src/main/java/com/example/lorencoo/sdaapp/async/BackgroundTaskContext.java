package com.example.lorencoo.sdaapp.async;

public interface BackgroundTaskContext {
    interface View {
        void showProgres();
        void hideProgres();
        void hideProgresRxJava();
        void updateProgress(Integer progress);
        void showOutput(String output);
    }
    interface Task{
        void count(Integer from,Integer to);
    }
    interface Presenter{
        void countWithRxJava(Integer from, Integer to);
        void clear();
    }
}
