package com.example.lorencoo.sdaapp.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.example.lorencoo.sdaapp.activity.FragmentStaticHostActivity;
import com.example.lorencoo.sdaapp.activity.MainActivity;

import timber.log.Timber;

/**
 * Created by lorencoo on 07.03.2018.
 */

public class ServicesCountTo30 extends IntentService {

    public static final int countTo = 30;
    public static final String BROADCAST_COUNT_KEY = "key used to send counter to progressBar via broadcast";
    public static final String BROADCAST_ACTION_COUNT_SERVICE = "com.example.lorencoo.sdaapp.activity.COUNT_SERVICE_ACTION";

    public ServicesCountTo30() {
        super("ServicesCountTo30");
        Timber.d("Constructor");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Timber.d("Count to 30 services created");
        startCounting();
    }

    private void startCounting() {
        for (int i = 0; i < countTo; i++) {
            setBroadcastSendToProgresBar(i);
            Timber.d(String.valueOf(i));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void setBroadcastSendToProgresBar(int i) {
        Intent intentSendBroadcastFromServiceTOProgresBar = new Intent();
        intentSendBroadcastFromServiceTOProgresBar.setAction(ServicesCountTo30.BROADCAST_ACTION_COUNT_SERVICE);
        intentSendBroadcastFromServiceTOProgresBar.putExtra(BROADCAST_COUNT_KEY, i + 1);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentSendBroadcastFromServiceTOProgresBar);
    }
}
