package com.example.lorencoo.sdaapp.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.lorencoo.sdaapp.MyPageAdapter;
import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.fragments.FirstFragment;
import com.example.lorencoo.sdaapp.fragments.SecondFragment;
import com.example.lorencoo.sdaapp.fragments.ThirdFragment;
import com.example.lorencoo.sdaapp.recivers.MyBroadcastReceiver;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PagesActivity extends AppCompatActivity {
    @BindView(R.id.pages_paper)
    ViewPager viewPager;

    @BindView(R.id.pages_tabs)
    TabLayout tab_layout;


    private MyBroadcastReceiver receiver;
    private List<Fragment> fragmentList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pages2);
        ButterKnife.bind(this);

        setupViewerPager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewPager.setCurrentItem(fragmentList.size()-1);
        registerAirplaneModeChangeReceiver();
    }
    @Override
    protected void onStop() {
        super.onStop();
        unregisterAirplaneModeChangeReceiver();
    }

    private void setupViewerPager() {
        fragmentList.add(new FirstFragment());
        fragmentList.add(new SecondFragment());
        fragmentList.add(new ThirdFragment());

        viewPager.setAdapter(new MyPageAdapter(this,getSupportFragmentManager(), fragmentList));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Notifies when a page was scrolled to (selected)
                Toast.makeText(getApplicationContext(), String.format("Hey! This is page %d", position + 1), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tab_layout.setupWithViewPager(viewPager);
    }
    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }
    private void unregisterAirplaneModeChangeReceiver() {
        unregisterReceiver(receiver);
    }

    private void registerAirplaneModeChangeReceiver() {
        receiver = new MyBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(receiver, intentFilter);
    }
}
