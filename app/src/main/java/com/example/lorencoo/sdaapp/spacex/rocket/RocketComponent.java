package com.example.lorencoo.sdaapp.spacex.rocket;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {RocketModule.class})
public interface RocketComponent {
    void inject(RocketActivity rocketActivity);
}
