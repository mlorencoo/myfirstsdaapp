package com.example.lorencoo.sdaapp.data.spacex_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Rocket {
 public String id;
 public String name;
 public String type;
 @SerializedName("payload_weights")
 @Expose
 public List<PayloadWeight> payloadWeights = new ArrayList<>();
 public String description;

 @Expose(serialize = false)
 String asdadasd;

}
