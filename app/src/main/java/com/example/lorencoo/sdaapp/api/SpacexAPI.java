package com.example.lorencoo.sdaapp.api;

public interface SpacexAPI {

    String BASE_URL = "http://api.spacexdata.com/v2";

    String COMPANY_INFO = BASE_URL + "/info";
    String ROCKET_INFO = BASE_URL + "/rockets";
}
