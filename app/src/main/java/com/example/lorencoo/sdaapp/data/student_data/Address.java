package com.example.lorencoo.sdaapp.data.student_data;

import android.arch.persistence.room.Entity;

@Entity(primaryKeys = {"street", "city","number"})
public class Address {

    public String street;
    public String city;
    public int number;

}
