package com.example.lorencoo.sdaapp.data.student_data;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsManager {
    private static final String FILE_NAME = "com.example.lorencoo.sdaapp.data.PREFS_FILE";
    private static final String STUDENTS_COUNT = "students_count";
    private SharedPreferences sharedPreferences;

    public PrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void savedStudentCount(int count) {
        sharedPreferences
                .edit()
                .putInt(STUDENTS_COUNT, count)
                .apply();
    }

    public int getStudentsCount() {
        return sharedPreferences.getInt(STUDENTS_COUNT, 0);
    }
}
