package com.example.lorencoo.sdaapp.api;

import com.example.lorencoo.sdaapp.data.spacex_data.CompanyInfo;
import com.example.lorencoo.sdaapp.data.spacex_data.Rocket;
import com.example.lorencoo.sdaapp.data.spacex_data.Launch;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    String BASE_URL = "http://api.spacexdata.com/v2/";

    @GET("info")
    Flowable<CompanyInfo> getCompanyInfo();

    @GET("rockets")
    Single<List<Rocket>> getRockets();

    @GET("launches")
    Single<List<Launch>> getLaunchesByYear(@Query("launch_year") int launchYear);

    @GET("launches")
    Single<List<Launch>> getLaunchesByYearAndId(@Query("launch_year") int launchYear,@Query("rocket_id") String rocketId);
}
