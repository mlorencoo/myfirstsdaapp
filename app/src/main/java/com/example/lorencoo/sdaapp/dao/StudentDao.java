package com.example.lorencoo.sdaapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.lorencoo.sdaapp.data.student_data.Student;

import java.util.List;

@Dao
public interface StudentDao {

    @Query("SELECT * FROM Student")
    public Student[] loadAllStudents();

    @Query("SELECT * FROM Student WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    Student findByName(String first, String last);

    @Query("SELECT * FROM Student WHERE first_name LIKE :search "
            + "OR last_name LIKE :search")
    public List<Student> findStudentWithName(String search);

    @Query("SELECT *FROM Student WHERE id LIKE :id LIMIT 1")
    Student getStudentById(int id);

    @Query("SELECT *FROM Student")
    public abstract List<Student> getAll();

    @Insert
    long insert(Student student);

    @Update
    public int updateStudents(Student student);

    @Delete
    void delete(Student student);
}
