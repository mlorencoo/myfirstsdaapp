package com.example.lorencoo.sdaapp.students.changeStudentData;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.SdaAplication;
import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.Student;
import com.example.lorencoo.sdaapp.students.StudentModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewStudent extends AppCompatActivity implements AddNewStudentContract.View {
    @BindView(R.id.edit_student_coordinator)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.etStudentName)
    EditText etStudentName;
    @BindView(R.id.etStudentSurname)
    EditText etStudentSurname;
    @BindView(R.id.etStudentAccountValue)
    EditText etStudentAccountValue;
    @BindView(R.id.edStudentCity)
    EditText edStudentCity;
    @BindView(R.id.edStudentStreet)
    EditText edStudentStreet;
    @BindView(R.id.edStudentNumberAddress)
    EditText edStudentNumberAddress;
    @BindView(R.id.rbStudentDrunk)
    RadioButton rbStudentDrunk;
    @BindView(R.id.spinnerStudentGrade)
    Spinner spinnerStudentGrade;

    private boolean isDrunkSelected;

    @Inject
    AddNewStudentContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_student);
        ButterKnife.bind(this);

        ((SdaAplication) getApplication()).getAppComponent()
                .plusAddNew(new AddNewStudentModule(this))
                .inject(this);

//        presenter= new AddNewStudentPresenter(this,SdaAplication.getDb().studentDao());

        presenter.handleStudent(getIntent().getIntExtra("idikStudenta", -1));

    }

    @OnClick(R.id.rbStudentDrunk)
    public void onIsDrunkClick() {
        isDrunkSelected = !isDrunkSelected;
        rbStudentDrunk.setChecked(isDrunkSelected);
    }


    @OnClick(R.id.btStudentSaveStudent)
    public void saveStudent() {
        //todo add/update student
        presenter.onSaveClick(
                etStudentName.getText().toString(),
                etStudentSurname.getText().toString(),
                spinnerStudentGrade.getSelectedItemPosition(),
                etStudentAccountValue.getText().toString(),
                rbStudentDrunk.isChecked(),
                edStudentCity.getText().toString(),
                edStudentStreet.getText().toString(),
                edStudentNumberAddress.getText().toString()
        );

    }

    @Override
    public void showStudentData(Student student) {
        etStudentName.setText(student.name);
        etStudentSurname.setText(student.surname);
        etStudentAccountValue.setText(String.valueOf(  student.accountValue));
        spinnerStudentGrade.setSelection(student.grade);
        rbStudentDrunk.setChecked(student.isDrunk);
        edStudentCity.setText(student.address.city);
        edStudentStreet.setText(student.address.street);
        edStudentNumberAddress.setText(String.valueOf( student.address.number));
    }
    @Override
    public void showMissingAddress() {
        Snackbar.make(
                coordinatorLayout,
                getString(R.string.missing_address),
                Snackbar.LENGTH_SHORT
        ).show();
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void showError() {
        Snackbar.make(
                coordinatorLayout,
                getString(R.string.student_add_error),
                Snackbar.LENGTH_SHORT
        ).show();
    }
}
