package com.example.lorencoo.sdaapp.data.spacex_data;

import com.google.gson.annotations.SerializedName;

public class CompanyInfo {
    String name;
    String founder;
    int employees;
    int vehicles;
    @SerializedName("launch_sites")
    int launchSites;
    Headquarter headquarters;
    String summary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public int getVehicles() {
        return vehicles;
    }

    public void setVehicles(int vehicles) {
        this.vehicles = vehicles;
    }

    public int getLaunchSites() {
        return launchSites;
    }

    public void setLaunchSites(int launchSites) {
        this.launchSites = launchSites;
    }

    public Headquarter getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(Headquarter headquarters) {
        this.headquarters = headquarters;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
