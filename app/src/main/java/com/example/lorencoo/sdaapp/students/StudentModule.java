package com.example.lorencoo.sdaapp.students;

import com.example.lorencoo.sdaapp.dao.StudentDao;
import com.example.lorencoo.sdaapp.data.student_data.PrefsManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StudentModule {

    private StudentsContract.View view;

    public StudentModule(StudentsContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    StudentsContract.Presenter provideStudentPresenter(PrefsManager prefsManager, StudentDao studentDao ){
        return new StudentsPresenter(view,prefsManager,studentDao);
    }
}
