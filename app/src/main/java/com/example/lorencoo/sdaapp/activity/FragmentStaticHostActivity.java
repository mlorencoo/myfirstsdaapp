package com.example.lorencoo.sdaapp.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.fragments.FragmentShowText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;

/**
 * Created by lorencoo on 24.02.2018.
 */

public class FragmentStaticHostActivity extends MenuItemsToSdaBar {

    public static final String BROADCAST_KEY = "key used to send string via broadcast";

    public static final String BROADCAST_NOTIFICATION_ACTION = "key used to send string via broadcast";

    public static final int notifyID = 123131;
    public static final String CHANNEL_ID = "my_channel_0123131";

    public static final String ACTION_SNOOZE = "com.example.lorencoo.sdaapp.activity.ACTION_SNOOZE";

    private BroadcastReceiver getLocalBroadcastNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onLocalBroadcastNotificationReciver();
        }
    };


    @BindView(R.id.inputTextFragmentXml)
    EditText inputTextFragmentXml;

    String textToInput = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_static_host);
        ButterKnife.bind(this);

        registerForLocalReceiverNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterLocalReceiverNotification();
    }

    @OnClick(R.id.putTextFragmentButtonXml)
    public void putEditTextToFragmnet() {
        FragmentShowText comunicator1 = (FragmentShowText) getSupportFragmentManager().findFragmentById(R.id.fragment_first_xml);
        FragmentShowText comunicator2 = (FragmentShowText) getSupportFragmentManager().findFragmentById(R.id.fragment_second_xml);
        String tempTextInput = inputTextFragmentXml.getText().toString();
        textToInput = textToInput + " " + tempTextInput;
        comunicator1.comunicate(textToInput);
        comunicator1.changeColor(getResources().getColor(R.color.colorAccent));
        comunicator2.changeColor(getResources().getColor(R.color.colorPrimary));
        comunicator2.comunicate("Wow it's working!!!");
        inputTextFragmentXml.setText("");
    }

    @OnClick(R.id.btFragmentStaticXML)
    public void sendBroadcastData() {
        Intent intentSendBroadcast = new Intent();
        intentSendBroadcast.setAction(MainActivity.BROADCAST_ACTION);
        intentSendBroadcast.putExtra(BROADCAST_KEY, "dupa");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentSendBroadcast);
    }

    @OnClick(R.id.btNotificationFragmentStaticXML)
    public void setBroadcastNotification() {
        Intent intentSendNotification = new Intent();
        intentSendNotification.setAction(FragmentStaticHostActivity.BROADCAST_NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentSendNotification);
    }

    private void registerForLocalReceiverNotification() {
        IntentFilter intentFilterNotification = new IntentFilter(FragmentStaticHostActivity.BROADCAST_NOTIFICATION_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(getLocalBroadcastNotification, intentFilterNotification);
    }

    private void unregisterLocalReceiverNotification() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(getLocalBroadcastNotification);
    }

    private void onLocalBroadcastNotificationReciver() {
        createNotification();
    }

    private void createNotification() {
        Intent resultIntent = new Intent(this, PagesActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent snoozeIntent = new Intent(this, MainActivity.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        }
        PendingIntent snoozePendingIntent = PendingIntent.getActivity(this, 0, snoozeIntent, 0);


        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_message))
                .setSmallIcon(R.drawable.ic_folder_black_24dp)
                .setContentIntent(resultPendingIntent)
                .addAction(R.drawable.phone_setings, getString(R.string.snozee), snoozePendingIntent)
                .setAutoCancel(true);


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }
        if (mNotificationManager != null) {
            mNotificationManager.notify(notifyID, notification.build());
        }

    }
}
