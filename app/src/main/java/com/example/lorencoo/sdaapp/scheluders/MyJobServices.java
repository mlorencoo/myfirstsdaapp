package com.example.lorencoo.sdaapp.scheluders;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.lorencoo.sdaapp.async.BackgroundTaskContext;
import com.example.lorencoo.sdaapp.services.ServicesCountTo30;

import org.reactivestreams.Subscription;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableSubscriber;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobServices extends JobService {
    private Disposable disposable;
    private MySceduleContract.View view;
    public static final int START_SJ=0;
    public static final int STOP_SJ=50;
    public static final String BROADCAST_SJ_KEY = "key used to send sj to progressBar via broadcast";
    public static final String BROADCAST_ACTION_SJ = "com.example.lorencoo.sdaapp.activity.MyJobServices";

    @Override
    public boolean onStartJob(JobParameters params) {
        counting(START_SJ, STOP_SJ,params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        disposable.dispose();
        return false;
    }

    private void counting( Integer from, Integer to,final JobParameters jobParameters ) {
        disposable = Flowable.create((FlowableEmitter<Integer> emitter) -> {
            Integer  start = from;
            while (start < to) {
                Timber.d(String.valueOf(start++));
                emitter.onNext(start);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.computation())
                .subscribe(currentProgress -> {
                    Log.e("JOB",currentProgress.toString());
                    setBroadcastSendToProgresBar(currentProgress);
                }, throwable -> {
                    jobFinished(jobParameters, false);
                }, () -> {
                    jobFinished(jobParameters, false);
                });
    }
    private void setBroadcastSendToProgresBar(int i) {
        Intent sendBroadcastFromSJTOProgresBar = new Intent();
        sendBroadcastFromSJTOProgresBar.setAction(BROADCAST_ACTION_SJ);
        sendBroadcastFromSJTOProgresBar.putExtra(BROADCAST_SJ_KEY, i );
        LocalBroadcastManager.getInstance(this).sendBroadcast(sendBroadcastFromSJTOProgresBar);
    }
}
