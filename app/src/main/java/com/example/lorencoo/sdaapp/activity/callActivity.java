package com.example.lorencoo.sdaapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.example.lorencoo.sdaapp.menues.MenuItemsToSdaBar;
import com.example.lorencoo.sdaapp.R;

public class callActivity extends MenuItemsToSdaBar {
    public static final String KEY_DATA="KEY_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
    }
    public void openNextCall(View view) {
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkBoxIAgree);

        Intent intentNextCall =new Intent(this,SecondNextCallActivity.class);
        intentNextCall.putExtra(KEY_DATA,checkBox.isChecked());
        startActivity(intentNextCall);
    }

}
