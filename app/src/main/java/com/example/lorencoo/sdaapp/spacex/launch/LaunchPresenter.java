package com.example.lorencoo.sdaapp.spacex.launch;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.sdaapp.api.Api;
import com.example.lorencoo.sdaapp.data.spacex_data.Launch;

import java.util.List;
import java.util.Observable;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

public class LaunchPresenter implements LaunchContract.Presenter, LifecycleObserver {
    private String rocketId;
    private LaunchContract.View view;
    private Api api;
    private List<Launch> launchList;
    private Realm realm;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LaunchPresenter(LaunchContract.View view, Api api) {
        this.view = view;
        this.api = api;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        realm=Realm.getDefaultInstance();
        getLaunchesByYearAndId(2018, rocketId);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
        realm.close();
        realm=null;
    }

    private void getLaunchesByYearAndId(int year, String id) {
        RealmResults<Launch> launchRealmResults = realm
                .where(Launch.class)
                .equalTo("rocketId", rocketId)
                .findAll();
        if (launchRealmResults.isEmpty()) {
            compositeDisposable.add(
                    api.getLaunchesByYearAndId(year, id)
                            .subscribeOn(Schedulers.io())
                            .flatMap(launches -> {
                                for (Launch launch : launches) {
                                    launch.setRocketId(rocketId);
                                }

                                return Single.just(launches);
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    launches -> {
                                        realm.executeTransaction(realm -> realm.copyToRealmOrUpdate(launches));
                                        view.showLaunches(launches);
                                    },
                                    throwable -> view.showError()
                            )
            );
        } else {
            view.showLaunches(launchRealmResults);
        }

    }

    @Override
    public void setRocketId(String rocketId) {
        this.rocketId = rocketId;
    }
}
