package com.example.lorencoo.sdaapp.async;

import android.os.AsyncTask;

import timber.log.Timber;

public class MyAsyncTask extends AsyncTask<Integer,Integer,Void> {
    @Override
    protected Void doInBackground(Integer... integers) {
        int i = integers[0];
        while ( i < 40) {
            Timber.d("%s Never ending services counted", String.valueOf(i++));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
