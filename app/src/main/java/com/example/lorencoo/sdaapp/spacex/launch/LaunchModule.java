package com.example.lorencoo.sdaapp.spacex.launch;

import com.example.lorencoo.sdaapp.api.Api;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketContract;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class LaunchModule {

    private LaunchContract.View view;

    public LaunchModule(LaunchContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    LaunchContract.Presenter provideLaunchPresenter(Retrofit retrofit ){
        return new LaunchPresenter(view, retrofit.create(Api.class));
    }
}
