package com.example.lorencoo.sdaapp.spacex.launch;

import com.example.lorencoo.sdaapp.spacex.rocket.RocketActivity;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {LaunchModule.class})
public interface LaunchComponent {
    void inject(LaunchActivity launchActivity);
}
