package com.example.lorencoo.sdaapp.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.SdaAplication;
import com.example.lorencoo.sdaapp.data.student_data.Student;
import com.example.lorencoo.sdaapp.students.changeStudentData.AddNewStudent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StudentActivity extends AppCompatActivity implements StudentsContract.View,
        StudentsContract.OnClickListenerRemove {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.rvStudents)
    RecyclerView rvStudents;

    @BindView(R.id.students_coordinator)
    CoordinatorLayout studentsCoordinator;

    private StudentsAdapter studentsAdapter;
    private Snackbar snackbar;

    @Inject
    StudentsContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_main);

        ((SdaAplication) getApplication()).getAppComponent()
                .plus(new StudentModule(this))
                .inject(this);


        ButterKnife.bind(this);

        setupRecycler();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getStudentsFromRoom();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.saveStudentCount();

    }

    @OnClick(R.id.fab)
    public void clickOnFabAddStudent() {
        startActivity(new Intent(this, AddNewStudent.class));
    }

    @Override
    public void updateList(List<Student> students) {
        studentsAdapter.updateStudentList(students);
    }

    @Override
    public void showStudentRemoved() {
        if (snackbar == null) {
            snackbar = Snackbar.make(
                    studentsCoordinator,
                    R.string.student_removed,
                    Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.undo), view -> presenter.onSnackBarActionClick())
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            presenter.onSnackBarDismissed();
                        }
                    });
        }
        snackbar.show();
    }

    @Override
    public void undoRemoval(int position, Student student) {
        studentsAdapter.undoRemoval(position, student);

    }

    @Override
    public void showToast() {
        Toast.makeText(this, "blrble toasta robie se", Toast.LENGTH_SHORT).show();
    }

    private void setupRecycler() {

        rvStudents.setLayoutManager(new LinearLayoutManager(this));

        studentsAdapter = new StudentsAdapter(this, new StudentsContract.Listener2() {
            @Override
            public void onClick(Student student) {
                presenter.on2Click();
            }
        });

        rvStudents.setAdapter(studentsAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvStudents.addItemDecoration(itemDecoration);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.recycler_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.menu_item_add_student == item.getItemId()) {
            startActivity(new Intent(this, AddNewStudent.class));
        }
        return true;
    }


    @Override
    public void removeStudent(Student student, int position) {
        presenter.removeStudent(student, position);
    }
}
