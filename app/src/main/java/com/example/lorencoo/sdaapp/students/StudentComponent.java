package com.example.lorencoo.sdaapp.students;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {StudentModule.class})
public interface StudentComponent {
    void inject(StudentActivity studentActivity );
}
