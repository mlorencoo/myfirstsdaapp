package com.example.lorencoo.sdaapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * Created by lorencoo on 09.03.2018.
 */

public class BinderService extends Service {

    private  MyBinder binder = new MyBinder();
    private Random numberGenerete = new Random();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class MyBinder extends Binder{
        public BinderService getService(){
            return BinderService.this;
        }
    }
    public String greet(){
        return String.format("your lucky number is %d",numberGenerete.nextInt(100));
    }
}
