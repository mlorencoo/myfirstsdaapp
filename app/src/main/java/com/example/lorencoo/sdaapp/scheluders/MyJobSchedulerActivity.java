package com.example.lorencoo.sdaapp.scheluders;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.lorencoo.sdaapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyJobSchedulerActivity extends AppCompatActivity implements MySceduleContract.View {
    public static final int MY_JOB_SCHEDULER_ID = 123;
    @BindView(R.id.progressJobScheduler)
    ProgressBar progressJobScheduler;

    @BindView(R.id.tvJobscheduler)
    TextView tvJobscheduler;

    @OnClick(R.id.btJobSchedule)
    public void onBtClick() {
        presenter.onButtonClick();
    }

    private MySceduleContract.Presenter presenter;

    private BroadcastReceiver getLBSendSJCounterToProgresBar = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onReceive(Context context, Intent intent) {
            onLBcounterSJToProgresBar(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_job_scheduler);
        ButterKnife.bind(this);
        presenter = new MyschedulerPresenter(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onStart() {
        super.onStart();
        registerForLocalReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterForLocalReceiver();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void scheduleJob() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.schedule(
                    new JobInfo.Builder(
                            MY_JOB_SCHEDULER_ID, new ComponentName(this, MyJobServices.class))
                            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                            .setRequiresCharging(true)
                            .build()
            );
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onLBcounterSJToProgresBar(Intent intent) {
        int progress = intent.getIntExtra(MyJobServices.BROADCAST_SJ_KEY, 0);
        float procentProgress = ((float) progress / (float) MyJobServices.STOP_SJ) * 100;
        String prcnt = String.format("%.2f", procentProgress);
        tvJobscheduler.setText(String.valueOf(prcnt + " %"));
        progressJobScheduler.setVisibility(View.VISIBLE);
        progressJobScheduler.setMax(MyJobServices.STOP_SJ);
        progressJobScheduler.setProgress(progress);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void registerForLocalReceiver() {
        IntentFilter intentFilter = new IntentFilter(MyJobServices.BROADCAST_ACTION_SJ);
        LocalBroadcastManager.getInstance(this).registerReceiver(getLBSendSJCounterToProgresBar, intentFilter);
    }

    private void unregisterForLocalReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(getLBSendSJCounterToProgresBar);
    }
}
