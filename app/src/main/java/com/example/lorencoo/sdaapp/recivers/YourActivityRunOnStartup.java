package com.example.lorencoo.sdaapp.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.lorencoo.sdaapp.activity.MainActivity;

import java.util.Objects;

/**
 * Created by lorencoo on 28.02.2018.
 */

public class YourActivityRunOnStartup extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), Intent.ACTION_BOOT_COMPLETED)) {
            Intent i = new Intent(context, MainActivity.class);
            context.startActivity(i);
        }
    }
}
