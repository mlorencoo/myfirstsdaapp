package com.example.lorencoo.sdaapp.spacex.launch;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.data.spacex_data.Launch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaunchAdapter extends RecyclerView.Adapter<LaunchAdapter.ViewHolder> {

    private List<Launch> launchList = new ArrayList<>();

    public void updateLaunches(List<Launch>launches ){
        launchList.clear();
        launchList.addAll(launches);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_laucher, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupLaucher(launchList.get(position));
    }

    @Override
    public int getItemCount() {
        return launchList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvLaunchDetials)
        TextView tvLaunchDetials;

        @BindView(R.id.imageLaunchSpacex)
        ImageView imageLaunchSpacex;

        @BindView(R.id.item_launch_date)
        TextView item_launch_date;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setupLaucher(Launch launch) {
            tvLaunchDetials.setText(launch.getDetails());

            Glide.with(itemView.getContext()).load(launch.getLink().getImageUrl()).into(imageLaunchSpacex);


            SimpleDateFormat inputFormat = new SimpleDateFormat(Launch.DATE_FORMAT, Locale.getDefault());
            SimpleDateFormat outputFormat = new SimpleDateFormat(Launch.DATE_OUTPUT, Locale.getDefault());

            try {
                Date date = inputFormat.parse(launch.getLaunchDateUtc());
                item_launch_date.setText(outputFormat.format(date));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
