package com.example.lorencoo.sdaapp.data.spacex_data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Link implements RealmModel {

    @PrimaryKey
    private  int likId;

    @SerializedName("mission_patch")
    private String imageUrl;

    public int getLikId() {
        return likId;
    }

    public void setLikId(int likId) {
        this.likId = likId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
