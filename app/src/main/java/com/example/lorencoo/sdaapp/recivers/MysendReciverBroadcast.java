package com.example.lorencoo.sdaapp.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by lorencoo on 28.02.2018.
 */

public class MysendReciverBroadcast extends BroadcastReceiver {
    private static final String TAG = "MysendReciverBroadcast";
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean status = intent.getBooleanExtra("extra", false);
        if (status) {
            String log = ("Action: " + intent.getAction() + "\n") +
                    "URI: " + intent.toUri(Intent.URI_INTENT_SCHEME) + "\n";
            Log.d(TAG, log);
        }
    }
}
