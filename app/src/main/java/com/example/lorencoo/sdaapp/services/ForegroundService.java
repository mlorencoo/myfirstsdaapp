package com.example.lorencoo.sdaapp.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.lorencoo.sdaapp.R;

import timber.log.Timber;


/**
 * Created by lorencoo on 08.03.2018.
 */

public class ForegroundService extends Service {

    public static final String NOTIFICATION_CHANNEL_ID = "channelIdForeground";
    public static final String NOTIFICATION_CHANNEL_NAME = "Channel Sda App - foreground";
    public static final String NOTIFICATION_ACTION_STOP_SERVICE = "com.sdaapp.action.notification_service_stop";
    public static final int NOTIFICATION_ID = 1519012310;

    private boolean shouldRun;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction() != null && intent.getAction().equals(ForegroundService.NOTIFICATION_ACTION_STOP_SERVICE)) {
            stopForeground(true);
            stopSelf();
        } else {
            startCounting();
            showNotification();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shouldRun = false;
        Timber.d("onDestroy");
    }

    private void startCounting() {
        HandlerThread handlerThread = new HandlerThread("Background thread");
        handlerThread.start();
        Handler handler = new Handler(handlerThread.getLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                // This will be done in the background thread
                shouldRun = true;
                int i = 1;
                while (shouldRun && i <= 40) {
                    Timber.d(String.valueOf(i++));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        });
    }

    private void showNotification() {
        Intent intent = new Intent(this, ForegroundService.class);
        intent.setAction(NOTIFICATION_ACTION_STOP_SERVICE);

        PendingIntent actionPendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_folder_black_24dp)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_message))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .addAction(R.drawable.ic_folder_black_24dp, "cancel", actionPendingIntent);

        // Create channel for Android 8.0 and above devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        // Starts the foreground notification that cannot be cancelled
        startForeground(NOTIFICATION_ID, builder.build());
    }
}
