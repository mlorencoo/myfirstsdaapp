package com.example.lorencoo.sdaapp.spacex.launch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.lorencoo.sdaapp.R;
import com.example.lorencoo.sdaapp.SdaAplication;
import com.example.lorencoo.sdaapp.api.Api;
import com.example.lorencoo.sdaapp.data.spacex_data.Launch;
import com.example.lorencoo.sdaapp.spacex.rocket.RocketAdapter;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class LaunchActivity extends AppCompatActivity implements LaunchContract.View {
    @BindView(R.id.spaces_launch_reycler)
    RecyclerView spaces_launch_reycler;

    private LaunchAdapter launchAdapter;

    @Inject
    LaunchContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        ButterKnife.bind(this);

        ((SdaAplication) getApplication()).getAppComponent()
                .plusLaunch(new LaunchModule(this))
                .inject(this);

//        Retrofit retrofit2 = new Retrofit.Builder()
//                // set base url for every request method defined in Api.class
//                .baseUrl(Api.BASE_URL)
//                // add Gson converter to deserialize JSON into POJO
//                .addConverterFactory(GsonConverterFactory.create(new Gson()))
//                // return Retrofit instance
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .build();
//
//        presenter = new LaunchPresenter(this, retrofit2.create(Api.class));

        launchAdapter = new LaunchAdapter();
        spaces_launch_reycler.setLayoutManager(new LinearLayoutManager(this));
        spaces_launch_reycler.setAdapter(launchAdapter);

        getIntentfromRocketActivity();
    }

    private void getIntentfromRocketActivity() {
        presenter.setRocketId(getIntent().getStringExtra(RocketAdapter.ROCKET_ID));
    }

    @Override
    public void showLaunches(List<Launch> launches) {
        spaces_launch_reycler.setVisibility(View.VISIBLE);
        launchAdapter.updateLaunches(launches);
    }

    @Override
    public void showError() {

    }


}
