package com.example.lorencoo.sdaapp.data.spacex_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Launch implements RealmModel {
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_OUTPUT = "yyyy MM dd";

    @Expose(deserialize = false,serialize = false)
    private String rocketId;

    @PrimaryKey
    @SerializedName("flight_number")
    private int flightNumber;

    @SerializedName("launch_year")
    private int launnchYear;
    @SerializedName("launch_date_utc")
    private String launchDateUtc;

    private String details;

    @SerializedName("links")
    private Link link;

    public static String getDateFormat() {
        return DATE_FORMAT;
    }

    public static String getDateOutput() {
        return DATE_OUTPUT;
    }

    public String getRocketId() {
        return rocketId;
    }

    public void setRocketId(String rocketId) {
        this.rocketId = rocketId;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public int getLaunnchYear() {
        return launnchYear;
    }

    public void setLaunnchYear(int launnchYear) {
        this.launnchYear = launnchYear;
    }

    public String getLaunchDateUtc() {
        return launchDateUtc;
    }

    public void setLaunchDateUtc(String launchDateUtc) {
        this.launchDateUtc = launchDateUtc;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }
}
