package com.example.lorencoo.sdaapp.fragments;

import android.graphics.Color;

/**
 * Created by lorencoo on 23.02.2018.
 */

public interface FragmentShowText {
    void comunicate(String textToShow);
    void changeColor(int colorToChange);
}
